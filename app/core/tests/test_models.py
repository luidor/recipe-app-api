from django.test import TestCase
from django.contrib.auth import get_user_model


class ModelTest(TestCase):
    def test_create_user_with_email_successful(self):
        """ Test Create a a new user with email"""
        email = 'test@gmail.com'
        password = 'testpass123'

        user = get_user_model().objects.create_user(
            email=email,
            password=password
        )
        self.assertEqual(user.email,email)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalize(self):
        email ='test@GMAIL.COM'

        user = get_user_model().objects.create_user(
                    email=email,
                    password="test1234"
                )
        self.assertEqual(user.email,email.lower())

    def test_new_user_invalid_email(self):

        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None,'test123')


    def test_create_new_suoer_user(self):
        """Test creating super user"""
        user = get_user_model().objects.create_superuser(
                'test@gmail.com',
                'test1234'
                )
        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)
