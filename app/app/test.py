from django.test import TestCase
from app.calc import add,subtract


class CalTests(TestCase):

    def test_add_number(self):
        """Test that two numbers are added together"""
        self.assertEqual(add(3,8),11)

    def test_subtract_number(self):
        """Test that two numbers are added together"""
        self.assertEqual(subtract(3,8),5)
